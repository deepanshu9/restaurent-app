import {
  All_RESTAURENT_SUCCESS,
  All_RESTAURENT_REQUEST,
  All_RESTAURENT_FAIL,
  RESTAURENT_DETAIL_REQUEST,
  RESTAURENT_DETAIL_SUCCESS,
  RESTAURENT_DETAIL_FAIL,
  OWN_RESTAURENT_REQUEST,
  OWN_RESTAURENT_SUCCESS,
  UPDATE_RESTAURENT_REQUEST,
  UPDATE_RESTAURENT_SUCCESS,
  UPDATE_RESTAURENT_FAIL,
  OWN_RESTAURENT_FAIL,
  CREATE_RESTAURENT_REQUEST,
  CREATE_RESTAURENT_SUCCESS,
  CREATE_RESTAURENT_FAIL,
  DELETE_RESTAURENT_REQUEST,
  DELETE_RESTAURENT_SUCCESS,
  DELETE_RESTAURENT_FAIL,
  All_OWNER_REQUEST,
  All_OWNER_SUCCESS,
  All_OWNER_FAIL,
  CLEAR_ERRORS,
} from "../constants/RestaurentConstant";

export const restaurentReducer = (state = { restaurent: [] }, action) => {
  switch (action.type) {
    case All_RESTAURENT_REQUEST:
    case OWN_RESTAURENT_REQUEST:
    case UPDATE_RESTAURENT_REQUEST:
    case CREATE_RESTAURENT_REQUEST:
    case DELETE_RESTAURENT_REQUEST:
      return {
        loading: true,
        restaurents:[]
      };
    case All_RESTAURENT_SUCCESS:
    case OWN_RESTAURENT_SUCCESS:
    case UPDATE_RESTAURENT_SUCCESS:
    case CREATE_RESTAURENT_SUCCESS:
    case DELETE_RESTAURENT_SUCCESS:
      return {
        loading: false,
        restaurent: action.payload.restaurents,
        restaurentCount: action.payload.restaurentsCount,
      };
    case All_RESTAURENT_FAIL:
    case OWN_RESTAURENT_FAIL:
    case UPDATE_RESTAURENT_FAIL: 
    case CREATE_RESTAURENT_FAIL:
    case DELETE_RESTAURENT_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export const restaurentDetailsReducer = (state = { restaurent: {} }, action) => {
  switch (action.type) {
    case RESTAURENT_DETAIL_REQUEST:
      return {
        loading: true,
        ...state
      };
    case RESTAURENT_DETAIL_SUCCESS:
      return {
        loading: false,
        restaurent: action.payload
      };
    case RESTAURENT_DETAIL_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export const allOwnerRestaurentReducer = (state = { restaurents: [] }, action) => {
  switch (action.type) {
    case All_OWNER_REQUEST:
      return {
        loading: true,
        ownerRestaurents:[]
      };
    case All_OWNER_SUCCESS:
      return {
        loading: false,
        ownerRestaurents: action.payload.restaurents,
      };
    case All_OWNER_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};


