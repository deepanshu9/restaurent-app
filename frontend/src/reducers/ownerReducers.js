import {
  OWNER_DETAIL_REQUEST,
  OWNER_DETAIL_FAIL,
  OWNER_DETAIL_SUCCESS,
  GET_ALL_USERS_REQUEST,
  GET_ALL_USERS_SUCCESS,
  GET_ALL_USERS_FAIL,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAIL,
  UPDATE_ROLE_REQUEST,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAIL,
  CLEAR_ERRORS,
} from "../constants/ownerConstant";

export const usersReducer = (state = { user: [] }, action) => {
  switch (action.type) {
    case GET_ALL_USERS_REQUEST:
    case DELETE_USER_REQUEST:
    case  UPDATE_ROLE_REQUEST:
      return {
        loading: true,
        users: [],
      };
    case GET_ALL_USERS_SUCCESS:
    case DELETE_USER_SUCCESS:
    case UPDATE_ROLE_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.payload,
      };
    case GET_ALL_USERS_FAIL:
    case DELETE_USER_FAIL:
    case UPDATE_ROLE_FAIL:
      return {
        loading: false,
        users: null,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export const ownerDetailsReducer = (state = { owner: {} }, action) => {
  switch (action.type) {
    case OWNER_DETAIL_REQUEST:
      return {
        loading: true,
        ...state,
      };
    case OWNER_DETAIL_SUCCESS:
      return {
        loading: false,
        owner: action.payload,
      };
    case OWNER_DETAIL_FAIL:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};
