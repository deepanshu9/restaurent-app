import React from 'react'
import './App.css';
import Header from "./components/layout/Header/Header"
import WebFont from "webfontloader";
import Footer from "./components/layout/Footer/Footer.js";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoginSignUp from "./components/User/LoginSignUp";
import Profile from "./components/User/Profile.js";
import { useDispatch, useSelector } from "react-redux";
import Home from "./components/Home/Home.js";
import { loadUser } from "./actions/userActions";
import UpdateProfile from "./components/User/UpdateProfile.js";
import UpdatePassword from './components/User/UpdatePassword.js';
import UserOptions from './components/layout/Header/UserOptions';
import RestaurentDetails from './components/Restaurent/RestaurentDetails';
import Restaurents from './components/Restaurent/Restaurents';
import CreateRestaurent from './components/Restaurent/CreateRestaurent.js';
import DashBoard from './components/Dashboard/Dashboard.js';
import AdminPanel from './components/Admin/AdminPanel.js';
import OwnerDetail from './components/Owner/OwnerDetail.js';
import AllUsersDetails from './components/Admin/AllUsersDetails.js'
function App() {
  const dispatch = useDispatch();

  const { isAuthenticated, user } = useSelector((state) => state.user);

  React.useEffect(() => {
    WebFont.load({
      google: {
        families: ["Roboto", "Droid Sans", "Chilanka"],
      },
    });
    dispatch(loadUser());
  }, [dispatch]);

  return (
    <Router>
      <Header/>
      {isAuthenticated && <UserOptions user={user} />}
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/restaurent/:uid/:rid" element={<RestaurentDetails/>} />
        <Route exact path="/restaurents" element={<Restaurents />} />
        {isAuthenticated ? (
          <Route path="/account" element={<Profile />} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/me/update" element={<UpdateProfile />} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/password/update" element={<UpdatePassword />} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/createRestaurent" element={<CreateRestaurent/>} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/admin" element={<AdminPanel/>} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/owner/:uid" element={<OwnerDetail/>} />
        ) : null}
        {isAuthenticated ? (
          <Route path="/admin/allUsers" element={<AllUsersDetails/>} />
        ) : null}
        <Route exact path="/login" element={<LoginSignUp />} />
        <Route exact path="/dashboard" element={<DashBoard />} />
      </Routes>
      <Footer/>
    </Router>
  );
}

export default App;
