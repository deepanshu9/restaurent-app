import axios from "axios";
import {
  All_RESTAURENT_SUCCESS,
  All_RESTAURENT_REQUEST,
  All_RESTAURENT_FAIL,
  RESTAURENT_DETAIL_REQUEST,
  RESTAURENT_DETAIL_SUCCESS,
  RESTAURENT_DETAIL_FAIL,
  OWN_RESTAURENT_REQUEST,
  OWN_RESTAURENT_SUCCESS,
  OWN_RESTAURENT_FAIL,
  UPDATE_RESTAURENT_REQUEST,
  UPDATE_RESTAURENT_SUCCESS,
  UPDATE_RESTAURENT_FAIL,
  CLEAR_ERRORS,
  CREATE_RESTAURENT_REQUEST,
  CREATE_RESTAURENT_SUCCESS,
  CREATE_RESTAURENT_FAIL,
  DELETE_RESTAURENT_REQUEST,
  DELETE_RESTAURENT_SUCCESS,
  DELETE_RESTAURENT_FAIL,
  All_OWNER_REQUEST,
  All_OWNER_SUCCESS,
  DELETE_OWNER_RESTAURENT_REQUEST,
  DELETE_OWNER_RESTAURENT_SUCCESS,
  DELETE_OWNER_RESTAURENT_FAIL,
  All_OWNER_FAIL,
} from "../constants/RestaurentConstant";

export const getActiveRestaurent =
  (city = "", price = [0, 2000]) =>
  async (dispatch) => {
    try {
      dispatch({
        type: All_RESTAURENT_REQUEST,
      });
      console.log(city, price);
      let link = `/api/v1/allrestaurents?city=${city}&AverageDiningPrice[gt]=${price[0]}&AverageDiningPrice[lt]=${price[1]}`;
      const { data } = await axios.get(link);
      console.log(data);

      dispatch({
        type: All_RESTAURENT_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: All_RESTAURENT_FAIL,
        payload: error,
      });
    }
  };

export const getRestaurentDetails = (uid, rid) => async (dispatch) => {
  try {
    dispatch({
      type: RESTAURENT_DETAIL_REQUEST,
    });

    const { data } = await axios.get(`/api/v1/restaurent/${uid}/${rid}`);
    dispatch({
      type: RESTAURENT_DETAIL_SUCCESS,
      payload: data.restaurent,
    });
  } catch (error) {
    dispatch({
      type: RESTAURENT_DETAIL_FAIL,
      payload: error.response.data.errors,
    });
  }
};

export const getOwnRestaurents = () => async (dispatch) => {
  try {
    dispatch({
      type: OWN_RESTAURENT_REQUEST,
    });
    const { data } = await axios.get("/api/v1/restaurents/me");
    console.log(data);

    dispatch({
      type: OWN_RESTAURENT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: OWN_RESTAURENT_FAIL,
      payload: error,
    });
  }
};

export const EditOwnRestaurents = (rid, restData) => async (dispatch) => {
  try {
    dispatch({ type: UPDATE_RESTAURENT_REQUEST });

    const config = { headers: { "Content-Type": "application/json" } };

    let updatedRestData = {
      rid,
      restaurentName: restData.get("restaurentName"),
      AverageDiningPrice: restData.get("AverageDiningPrice"),
      city: restData.get("city"),
      status: restData.get("status"),
    };

    console.log(updatedRestData);
    const { data } = await axios.patch(
      `/api/v1/editrestaurent`,
      updatedRestData,
      config
    );

    console.log(data.success);
    dispatch({ type: UPDATE_RESTAURENT_SUCCESS, payload: data.success });
  } catch (error) {
    dispatch({
      type: UPDATE_RESTAURENT_FAIL,
      payload: error.response.data.message,
    });
  }
};

export const createRestaurent = (restData) => async (dispatch) => {
  try {
    dispatch({ type: CREATE_RESTAURENT_REQUEST });

    const config = { headers: { "Content-Type": "application/json" } };

    let createRestaurentData = {
      restaurent: {
        restaurentName: restData.get("restaurentName"),
        images: { url: restData.get("restaurentImage") },
        city: restData.get("restaurentCity"),
        status: restData.get("restaurentStatus"),
        AverageDiningPrice: restData.get("restaurentAvgPrice"),
      },
    };
    console.log(createRestaurentData)
    const { data } = await axios.post(
      `/api/v1/restaurent/new`,
      createRestaurentData,
      config
    );

    console.log(data);

    dispatch({ type: CREATE_RESTAURENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CREATE_RESTAURENT_FAIL,
      payload: error.response.data.errors,
    });
  }
};
export const deleteRestaurent = (rid) => async (dispatch) => {
  try {
    dispatch({
      type: DELETE_RESTAURENT_REQUEST,
    });

    const deleteRestaurent={
      rid
    }
  
    console.log(rid)
    const { data } = await axios.post(
      `/api/v1/deleteRestaurent`,
      deleteRestaurent
    );
    dispatch({
      type: DELETE_RESTAURENT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DELETE_RESTAURENT_FAIL,
      payload: error.response.data.errors,
    });
  }
};

export const allOwnerRestaurentForAdmin = () => async (dispatch) => {
  try {
    dispatch({
      type: All_OWNER_REQUEST,
    });
    const { data } = await axios.get("/api/v1/restaurents");
    console.log(data);
    
    dispatch({
      type: All_OWNER_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: All_OWNER_FAIL,
      payload: error,
    });
  }
};

export const deleteOwnerRestaurentByAdmin = (uid,rid) => async (dispatch) => {
  try {
    dispatch({
      type: DELETE_RESTAURENT_REQUEST,
    });

    const deleteRestaurent={
      userId:uid,
      restId:rid
    }
    console.log(deleteRestaurent)
    const { data } = await axios.post(
      `/api/v1/deleteRestaurentByAdmin`,
      deleteRestaurent
    );
    dispatch({
      type: DELETE_RESTAURENT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DELETE_RESTAURENT_FAIL,
      payload: error.response.data.errors,
    });
  }
};

export const EditOwnRestaurentsByAdmin = (uid,rid, restData) => async (dispatch) => {
  try {
    dispatch({ type: UPDATE_RESTAURENT_REQUEST });

    const config = { headers: { "Content-Type": "application/json" } };

    let updatedRestData = {
      userId:uid,
      restId:rid,
      restaurentName: restData.get("restaurentName"),
      AverageDiningPrice: restData.get("AverageDiningPrice"),
      images: { url: restData.get("imageUrl") },
      city: restData.get("city"),
      status: restData.get("status"),
    };
    
    console.log(updatedRestData);
    const { data } = await axios.patch(
      `/api/v1/editRestaurentByAdmin`,
      updatedRestData,
      config
    );

    console.log(data.success);
    dispatch({ type: UPDATE_RESTAURENT_SUCCESS, payload: data.success });
  } catch (error) {
    dispatch({
      type: UPDATE_RESTAURENT_FAIL,
      payload: error.response.data.message,
    });
  }
};
// Clearing Error
export const clearErrors = () => async (dispatch) => {
  dispatch({
    type: CLEAR_ERRORS,
  });
};
