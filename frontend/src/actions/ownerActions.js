import {
    OWNER_DETAIL_REQUEST,
    OWNER_DETAIL_FAIL,
    OWNER_DETAIL_SUCCESS,
    CLEAR_ERRORS,
    GET_ALL_USERS_REQUEST,
    GET_ALL_USERS_SUCCESS,
    GET_ALL_USERS_FAIL,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAIL,
    UPDATE_ROLE_REQUEST,
    UPDATE_ROLE_SUCCESS,
    UPDATE_ROLE_FAIL
  } from "../constants/ownerConstant"
import axios from 'axios'


export const getOwnerDetails = (uid) => async (dispatch) => {
    try {
      dispatch({
        type: OWNER_DETAIL_REQUEST,
      });
      console.log(uid)
      const {data} = await axios.get(`/api/v1/admin/user/${uid}`);
      console.log(data)
      dispatch({
        type: OWNER_DETAIL_SUCCESS,
        payload: data.user,
      });
    } catch (error) {
      dispatch({
        type: OWNER_DETAIL_FAIL,
        payload: error,
      });
    }
  };

  export const getAllUsersByAdmin = () => async (dispatch) => {
    try {
      dispatch({
        type: GET_ALL_USERS_REQUEST,
      });
      const  {data}  = await axios.get("/api/v1/admin/users");
      
      
      dispatch({
        type: GET_ALL_USERS_SUCCESS,
        payload: data.user,
      });
    } catch (error) {
      dispatch({
        type: GET_ALL_USERS_FAIL,
        payload: error,
      });
    }
  };

  export const deleteUserByAdmin = (id) => async (dispatch) => {
    try {
      dispatch({
        type: DELETE_USER_REQUEST,
      });
      const {data} = await axios.delete(`/api/v1/admin/user/${id}`);
      console.log(data)
      dispatch({
        type: DELETE_USER_SUCCESS,
        payload: data.user,
      });
    } catch (error) {
      dispatch({
        type: DELETE_USER_FAIL,
        payload: error,
      });
    }
  };

  export const changeUserRoleByAdmin = (role,id) => async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ROLE_REQUEST });
  
      const config = { headers: { "Content-Type": "application/json" } };
  
      let updatedRestData = {
          role
      };
  
      console.log(updatedRestData);
      const { data } = await axios.put(
        `/api/v1/admin/user/${id}`,
        updatedRestData,
        config
      );
  
      console.log(data.success);
      dispatch({ type: UPDATE_ROLE_SUCCESS, payload: data.success });
    } catch (error) {
      dispatch({
        type: UPDATE_ROLE_FAIL,
        payload: error.response.data.message,
      });
    }
  };
  

  // Clearing Error
export const clearErrors = () => async (dispatch) => {
    dispatch({
      type: CLEAR_ERRORS,
    });
  };
  