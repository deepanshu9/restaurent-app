import React, { Fragment, useEffect } from "react";
import { CgMouse } from "react-icons/all";
import "./Home.css";

import MetaData from "../layout/MetaData";
import { useSelector, useDispatch } from "react-redux";
import Loader from "../layout/Loader/Loader";
import { useAlert } from "react-alert";
import { clearErrors, getActiveRestaurent } from "../../actions/RestaurentActions";
import RestaurentCard from "./RestaurentCard";

const Home = () => {
  const alert = useAlert();
  const dispatch = useDispatch();
  const { loading, error, restaurent} = useSelector(
    (state) => state.restaurents
  );
  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors())
    }
    dispatch(getActiveRestaurent());
  }, [dispatch, error,alert]);

  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <MetaData title="Ecommerce" />

          <div className="banner">
            <p>OPEN TABLE</p>
            <h1>Book your Table</h1>

            <a href="#container">
              <button>
                Scroll <CgMouse />
              </button>
            </a>
          </div>
          <h2 className="homeHeading">Restaurents</h2>
          <div className="products">
            {restaurent &&
              restaurent.map((rest) => <RestaurentCard  restaurent={rest} />)}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default Home;
