import React from "react";
import { Link } from "react-router-dom";

import "./Home.css";

const RestaurentCard = ({ restaurent }) => {
  return (
    
      <Link
        className="productCard"
        to={`/restaurent/${restaurent.owner}/${restaurent.rest._id}`}
      >
        <img src={restaurent.rest.images[0].url} alt="rest" />
        <p>{restaurent.rest.restaurentName}</p>
        <div>
          <div>{restaurent.rest.city}</div>
          <p>Average Dinning Price${restaurent.rest.AverageDiningPrice}</p>
        </div>
        <span>Status : {restaurent.rest.status}</span>
      </Link>
   
  );
};

export default RestaurentCard;
