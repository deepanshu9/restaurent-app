import React, { Fragment, useState } from "react";
import "./Header.css";
import { SpeedDial, SpeedDialAction } from "@material-ui/lab";
import DashboardIcon from "@material-ui/icons/Dashboard";
import Backdrop from "@material-ui/core/Backdrop";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { useAlert } from "react-alert";
import { logout } from "../../../actions/userActions";
const UserOptions = ({ user }) => {
  const [open, setOpen] = useState(false);
  const alert = useAlert();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const options = [
    { icon: <PersonIcon />, name: "Profile", func: account },
    { icon: <ExitToAppIcon />, name: "Logout", func: logoutUser },
  ];
  if (user.role === "owner" || user.role==="admin") {
    options.unshift({
      icon: <DashboardIcon />,
      name: "Dashboard",
      func: dashboard,
    });
    options.unshift({
      icon: <AddIcon />,
      name: "Create Restaurent",
      func: createRestaurent,
    });
  }
  if(user.role === "admin" ){
    options.unshift({
      icon: <AdminPanelSettingsIcon/>,
      name: "Admin",
      func: adminController,
    });
  }
  function createRestaurent(){
    navigate('/createRestaurent')
  }
  function dashboard() {
    navigate("/dashboard");
  }
  function adminController() {
    navigate("/admin")
  }
 
  function account() {
    navigate("/account");
  }
  
  function logoutUser() {
    dispatch(logout());
    navigate("/");
    alert.success("Logout SuccessFully");
  }
  return (
    <Fragment>
      <Backdrop open={open} style={{ zIndex: 10 }} />
      <SpeedDial
        className="speedDial"
        ariaLabel="SpeedDial tooltip example"
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        style={{ zIndex: 11 }}
        open={open}
        direction="down"
        icon={
          <img className="speedDialIcon" src="/Profile.png" alt="Profile" />
        }
      >
        {options.map((item) => (
          <SpeedDialAction
            className="speedDialIcon"
            key={item.name}
            icon={item.icon}
            tooltipTitle={item.name}
            onClick={item.func}
            tooltipOpen={window.innerWidth<=600?true:false}
          />
        ))}
      </SpeedDial>
    </Fragment>
  );
};

export default UserOptions;
