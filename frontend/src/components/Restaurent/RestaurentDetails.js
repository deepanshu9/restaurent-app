import React, { Fragment, useEffect, useState } from "react";
import "./RestaurentDetails.css";
import Carousel from "react-material-ui-carousel";
import { useSelector, useDispatch } from "react-redux";
import {
  clearErrors,
  getRestaurentDetails,
} from "../../actions/RestaurentActions";
import { useParams } from "react-router-dom";
import Loader from "../layout/Loader/Loader";
import { useAlert } from "react-alert";
import MetaData from "../layout/MetaData";

const RestaurentDetails = () => {
  const alert = useAlert();
  const { uid, rid } = useParams();
  const dispatch = useDispatch();
  const { restaurent, loading, error } = useSelector(
    (state) => state.restaurentDetail
  );
  useEffect(() => {
    dispatch(getRestaurentDetails(uid, rid));
  }, [dispatch, uid, rid]);

  console.log(restaurent);
  return (
    <Fragment>
      {!restaurent ? (
        <Loader />
      ) : (
        <Fragment>
          <MetaData title="Restaurent" />
          <div className="ProductDetails">
          <Carousel>
                {restaurent.images &&
                  restaurent.images.map((item, i) => (
                    <img
                      className="CarouselImage"
                      key={i}
                      src={item.url}
                      alt={`${i} Slide`}
                    />
                  ))}
              </Carousel>
              <div>
              <div className="detailsBlock-1">
                <h2>{restaurent.restaurentName}</h2>
                <p>Restaurent # {restaurent._id}</p>
              </div>
              <div className="detailsBlock-2">
                <span>Address: {restaurent.city}</span>
              </div>
              <div className="detailsBlock-3">
                <h5>Average Dinning Price: {`$${restaurent.AverageDiningPrice}`}</h5>
                <p>
                  Status:
                  <b className={restaurent.status ==="Inactive" ? "redColor" : "greenColor"}>
                    {restaurent.status}
                  </b>
                </p>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default RestaurentDetails;
