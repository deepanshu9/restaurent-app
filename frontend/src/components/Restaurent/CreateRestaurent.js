import React, { Fragment, useState } from "react";
import "./CreateRestaurent.css";
import { useDispatch } from "react-redux";
import {createRestaurent } from "../../actions/RestaurentActions";
const CreateRestaurent = () => {
  const dispatch = useDispatch();
  const [restName, setRestName] = useState("");
  const [restImage, setRestImage] = useState("");
  const [restCity, setRestCity] = useState("");
  const [status, setStatus] = useState("active");
  const [restAvgPrice, setRestAvgPrice] = useState();

  const createRestaurentHandler = (e) => {
    e.preventDefault();

    const myForm = new FormData();

    myForm.set("restaurentName", restName);
    myForm.set("restaurentImage", restImage);
    myForm.set("restaurentCity", restCity);
    myForm.set("restaurentStatus", status);
    myForm.set("restaurentAvgPrice", restAvgPrice);
    dispatch(createRestaurent(myForm));
  };
  return (
    <Fragment>
      <div className="container">
        <div className="FormBox">
          <h2>Add New Restaurent</h2>
          <form onSubmit={createRestaurentHandler}>
            <label>Image Url</label>
            <input
              type="text"
              value={restImage}
              onChange={(e) => setRestImage(e.target.value)}
            />
            <label>Restaurent Name</label>
            <input
              type="text"
              value={restName}
              onChange={(e) => setRestName(e.target.value)}
            />
            <label>City</label>
            <input
              type="text"
              value={restCity}
              onChange={(e) => setRestCity(e.target.value)}
            />{" "}
            <label>Average Dining Price </label>
            <input
              type="number"
              onChange={(e) => setRestAvgPrice(e.target.value)}
            />
            <label>Status</label>
            <select
            value={status}
              onChange={(e) => setStatus(e.target.value)}
            >
              <option value="active">active</option>
              <option value="Inactive">Inactive</option>
            </select>
            <input type="submit" value="Submit" />
          </form>
        </div>
      </div>
    </Fragment>
  );
};

export default CreateRestaurent;
