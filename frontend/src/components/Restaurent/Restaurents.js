import React, { Fragment, useEffect, useState } from "react";
import "./Restaurents.css";
import { useSelector, useDispatch } from "react-redux";
import {
  clearErrors,
  getActiveRestaurent,
} from "../../actions/RestaurentActions";
import Loader from "../layout/Loader/Loader";
import Slider from "@material-ui/core/Slider";
import { useAlert } from "react-alert";
import Typography from "@material-ui/core/Typography";
import MetaData from "../layout/MetaData";
import { useParams } from "react-router-dom";
import RestaurentCard from "../Home/RestaurentCard";

const cities = [
  "Gurgaon",
  "Delhi",
  "Jaipur",
  "Chandigarh",
  "Lucknow",
  "Punjab",
  "Ghaziabad",
];

const Restaurents = () => {
  const dispatch = useDispatch();
  const alert = useAlert();

  const [price, setPrice] = useState([0, 2000]);
  const [city, setCity] = useState("");
  const { restaurent, loading, error, restaurentCount } = useSelector(
    (state) => state.restaurents
  );

  const priceHandler = (event, newPrice) => {
    setPrice(newPrice);
  };
  console.log(price,city)

  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }

    dispatch(getActiveRestaurent());
  }, [dispatch, city, price, alert, error]);


  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <MetaData title="PRODUCTS -- ECOMMERCE" />
          <h2 className="productsHeading">Restaurent</h2>

          <div className="products">
            {restaurent &&
              restaurent.map((rest) => <RestaurentCard restaurent={rest} />)}
          </div>
          <div className="filterBox">
            <Typography>Price</Typography>
            <Slider
              value={price}
              onChange={priceHandler}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={0}
              max={2000}
            />

            <Typography>City</Typography>
            <ul className="categoryBox">
              {cities.map((city) => (
                <li
                  className="category-link"
                  key={city}
                  onClick={() => setCity(city)}
                >
                  {city}
                </li>
              ))}
            </ul>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default Restaurents;
