import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch } from "react-redux";
import "../Home/Home.css";
import { Button, Modal } from "react-bootstrap";
import {
  deleteOwnerRestaurentByAdmin,
  EditOwnRestaurentsByAdmin,
} from "../../actions/RestaurentActions";
import PersonIcon from "@material-ui/icons/Person";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import {useNavigate} from 'react-router-dom'
const OwnerRestaurentCard = ({ owner }) => {
  const dispatch = useDispatch();
  const navigate=useNavigate();
  const [modal, showModal] = useState(false);
  const [restName, setRestName] = useState();
  const [restImage, setRestImage] = useState();
  const [restCity, setRestCity] = useState();
  const [status, setStatus] = useState();
  const [restAvgPrice, setRestAvgPrice] = useState();

  const deleteOwnerRestaurent = (rid) => {
    dispatch(deleteOwnerRestaurentByAdmin(owner.user, rid));
  };
  const userInfoHandler=(owner)=>{
    console.log(owner.user)
    navigate(`/owner/${owner.user}`)
  }
  function EditRestaurent(rest) {
    setRestName(rest.restaurentName);
    setRestImage(rest.images[0].url);
    setRestCity(rest.city);
    setStatus(rest.status);
    setRestAvgPrice(rest.AverageDiningPrice);
    showModal(true);
  }
  const updateRestaurent = (rid) => {
    console.log(rid);
    const myForm = new FormData();
    myForm.set("imageUrl", restImage);
    myForm.set("restaurentName", restName);
    myForm.set("AverageDiningPrice", restAvgPrice);
    myForm.set("city", restCity);
    myForm.set("status", status);
    dispatch(EditOwnRestaurentsByAdmin(owner.user, rid, myForm));
  };
  return (
    <Fragment>
      {owner.restaurent &&
        owner.restaurent.map((rest) => (
          <div className="ownerRestaurentContainer">
            <div className="productCard">
              <span>
                <div onClick={()=>userInfoHandler(owner)}>
                  <PersonIcon />
                  <span>owner: {owner.user}</span>
                  <ArrowRightIcon />
                </div>
              </span>
              <img src={rest.images[0].url} alt="rest" />
              <p>{rest.restaurentName}</p>
              <div>
                <div>{rest.city}</div>
                <p>Average Dinning Price : ${rest.AverageDiningPrice}</p>
              </div>
              <span>Status : {rest.status}</span>
              <span>{rest._id}</span>
            </div>
            <div>
              <button class="button" onClick={() => EditRestaurent(rest)}>
                <span>EditRestaurent</span>
              </button>
              <button
                class="button"
                onClick={() => deleteOwnerRestaurent(rest._id)}
              >
                <span>Delete Restaurent</span>
              </button>
            </div>
            {modal && (
              <Modal
                show={() => showModal(true)}
                onHide={() => showModal(false)}
              >
                <Modal.Header closeButton>Edit Restaurent</Modal.Header>
                <Modal.Body>
                  <form>
                    <label>Image Url</label>
                    <input
                      type="text"
                      value={restImage}
                      onChange={(e) => setRestImage(e.target.value)}
                    />
                    <label>Restaurent Name</label>
                    <input
                      type="text"
                      value={restName}
                      onChange={(e) => setRestName(e.target.value)}
                    />
                    <label>City</label>
                    <input
                      type="text"
                      value={restCity}
                      onChange={(e) => setRestCity(e.target.value)}
                    />{" "}
                    <label>Average Dining Price </label>
                    <input
                      type="number"
                      value={restAvgPrice}
                      onChange={(e) => setRestAvgPrice(e.target.value)}
                    />
                    <label>Status</label>
                    <select onChange={(e) => setStatus(e.target.value)}>
                      <option value="active">active</option>
                      <option value="Inactive">Inactive</option>
                    </select>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={() => updateRestaurent(rest._id)}>
                    Update
                  </Button>
                </Modal.Footer>
              </Modal>
            )}
          </div>
        ))}
    </Fragment>
  );
};

export default OwnerRestaurentCard;
