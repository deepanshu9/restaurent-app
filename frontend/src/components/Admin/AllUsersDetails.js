import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getAllUsersByAdmin } from "../../actions/ownerActions";
import Loader from "../layout/Loader/Loader";
import Profile from "../../images/Profile.png";
import "./AdminPanel.css";
const AllUsersDetails = () => {
  const dispatch = useDispatch();
  const { loading, errors, users } = useSelector((state) => state.allUsers);
  useEffect(() => {
    dispatch(getAllUsersByAdmin());
  }, []);
  console.log(users);
  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          {users && (
            <div className="products">
              {users.map((user) => (
                <Link className="productCard" to={`/owner/${user._id}`}>
                  <img src={Profile} alt="rest" />
                  <p>Name: {user.name}</p>
                  <div>
                    <div>Email: {user.email}</div>
                    <p>Role: {user.role}</p>
                  </div>
                  <span>Created At :{user.createdAt}</span>
                </Link>
              ))}
            </div>
          )}
        </Fragment>
      )}
    </Fragment>
  );
};

export default AllUsersDetails;
