import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { allOwnerRestaurentForAdmin, clearErrors } from "../../actions/RestaurentActions";
import Loader from "../layout/Loader/Loader";
import MetaData from "../layout/MetaData";
import "./AdminPanel.css"
import { useNavigate } from "react-router";
import OwnerRestaurentCard from './OwnerRestaurentCard.js';

const AdminPanel = () => {
  const dispatch = useDispatch();
  const navigate=useNavigate();
  const { loading, ownerRestaurents,error } = useSelector(
    (state) => state.adminPanel
  );
 

  useEffect(() => {
    if (error) {
        alert.error(error);
        dispatch(clearErrors());
      }
  
      dispatch(allOwnerRestaurentForAdmin());
  }, []);
  
    const getAllUsers=()=>{
      navigate("/admin/allUsers")
    }
     return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <MetaData title="admin panel" />
          <div className="btnContainer">
              <button className="btn" onClick={getAllUsers}>Get All Users</button>
          </div>
          <h2 className="productsHeading">Owner's Restaurent</h2>

          <div className="products">
            {ownerRestaurents &&
              ownerRestaurents.map((owner) => <OwnerRestaurentCard owner={owner} />)}
          </div>
          
        </Fragment>
      )}
    </Fragment>
  );
};

export default AdminPanel;
