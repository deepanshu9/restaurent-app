import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../Home/Home.css";
import { useDispatch } from "react-redux";
import { Button, Modal } from "react-bootstrap";
import { EditOwnRestaurents,deleteRestaurent } from "../../actions/RestaurentActions";
const OwnRestaurentCard = ({ restaurent, userId }) => {
  const dispatch = useDispatch();
  const [modal, showModal] = useState(false);
  const [restName, setRestName] = useState(restaurent.restaurentName);
  const [restImage, setRestImage] = useState(restaurent.images[0].url);
  const [restCity, setRestCity] = useState(restaurent.city);
  const [status, setStatus] = useState(restaurent.status);
  const [restAvgPrice, setRestAvgPrice] = useState(
    restaurent.AverageDiningPrice
  );
  
  function EditRestaurent() {
    showModal(true);
  }
  function DeleteRestaurent() {
    dispatch(deleteRestaurent(restaurent._id));
  }
  const updateRestaurent=()=>{
  
    const myForm = new FormData();
    myForm.set("imageUrl", restImage);
    myForm.set("restaurentName",restName );
    myForm.set("AverageDiningPrice",restAvgPrice );
    myForm.set("city",restCity );
    myForm.set("status",status );
    dispatch(EditOwnRestaurents(restaurent._id,myForm));
  }
  
  return (
    <div className="restcontainer">
      <Link
        className="productCard"
        to={`/restaurent/${userId}/${restaurent._id}`}
      >
        <img src={restaurent.images[0].url} alt="rest" />
        <p>{restaurent.restaurentName}</p>
        <div>
          <div>{restaurent.city}</div>
          <p>Average Dinning Price : ${restaurent.AverageDiningPrice}</p>
        </div>
        <span>Status : {restaurent.status}</span>
      </Link>
      <div>
        <button class="button" onClick={EditRestaurent}>
          <span>EditRestaurent</span>
        </button>
        <button class="button" onClick={DeleteRestaurent}>
          <span>Delete Restaurent</span>
        </button>
      </div>
      {modal && (
        <Modal show={() => showModal(true)} onHide={() => showModal(false)}>
          <Modal.Header closeButton>Edit Restaurent</Modal.Header>
          <Modal.Body>
            <form>
              <label>Image Url</label>
              <input
                type="text"
                id="fname"
                value={restImage}
                onChange={(e) => setRestImage(e.target.value)}
              />
              <label>Restaurent Name</label>
              <input
                type="text"
                id="fname"
                value={restName}
                onChange={(e) => setRestName(e.target.value)}
              />
              <label>City</label>
              <input
                type="text"
                id="lname"
                value={restCity}
                onChange={(e) => setRestCity(e.target.value)}
              />{" "}
              <label>Average Dining Price </label>
              <input
                type="number"
                id="lname"
                value={restAvgPrice}
                onChange={(e) => setRestAvgPrice(e.target.value)}
              />
              <label>Status</label>
              <select id="country" onChange={(e) => setStatus(e.target.value)}>
                <option value="active">active</option>
                <option value="Inactive">Inactive</option>
              </select>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={() => this.handleModal()}>Close</Button>
            <Button onClick={updateRestaurent}>Update</Button>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
};

export default OwnRestaurentCard;
