import React, { Fragment, useEffect } from "react";
import { useSelector,useDispatch } from "react-redux";
import {useNavigate} from 'react-router-dom'
import { getOwnRestaurents } from "../../actions/RestaurentActions";
import OwnRestaurentCard from "./OwnRestaurentCard";
import Loader from '../layout/Loader/Loader'
import "../Home/Home.css"
const Dashboard = () => {
  const {user,isAuthenticated } = useSelector((state) => state.user);
  const { restaurent, loading, error, restaurentCount } = useSelector(
    (state) => state.restaurents
  );

  const navigate=useNavigate();
  const dispatch=useDispatch();


  useEffect(() => {
    if (isAuthenticated === false) {
      navigate("/login");
    }
    dispatch(getOwnRestaurents())
  }, [navigate, isAuthenticated]);
  return (
      <Fragment>
          {loading ? <Loader/> : (
              <Fragment>
              <div>
                <div>
                  <h2 className="productsHeading">Your Restaurents</h2>
                  <div className="products">
                    {restaurent &&
                      restaurent.map((rest) => <OwnRestaurentCard  restaurent={rest} userId={user._id}/>)}
                  </div>
                </div>
              </div>
            </Fragment>
          )}
    
    </Fragment>
  );
};

export default Dashboard;
