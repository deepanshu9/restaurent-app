import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  getOwnerDetails,
  clearErrors,
  deleteUserByAdmin,
  changeUserRoleByAdmin,
} from "../../actions/ownerActions";
import { useDispatch, useSelector } from "react-redux";
import Profile from "../../images/Profile.png";
import "./OwnerDetail.css";
import Loader from "../layout/Loader/Loader";
const OwnerDetail = () => {
  const { uid } = useParams();
  const dispatch = useDispatch();

  const { owner, loading, error } = useSelector((state) => state.ownerDetail);

  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }
    dispatch(getOwnerDetails(uid));
  }, []);
  const deleteUser = (id) => {
    dispatch(deleteUserByAdmin(id));
  };
  const changeRoleHandler = (e) => {
    const role = e.target.value;
    const id = owner._id;
    if (role !== "Change Role") {
      dispatch(changeUserRoleByAdmin(role, id));
    }
  };
  return (
    <Fragment>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <div className="profileContainer">
            <div>
              <h1>Profile</h1>
              <img src={Profile} alt={owner.name} />
            </div>
            <div>
              <div>
                <h4>Full Name</h4>
                <p>{owner.name}</p>
              </div>
              <div>
                <h4>Email</h4>
                <p>{owner.email}</p>
              </div>
              <div>
                <h4>Joined On</h4>
                <p>{String(owner.createdAt).substr(0, 10)}</p>
              </div>
              <div>
                <h4>Role</h4>
                <p>{owner.role}</p>
              </div>
              <div>
                <button className="btn" onClick={() => deleteUser(owner._id)}>
                  Delete User
                </button>
                <select onChange={changeRoleHandler}>
                  <option>Change Role</option>
                  <option value="admin">admin</option>
                  <option value="owner">owner</option>
                  <option value="user">user</option>
                </select>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};
export default OwnerDetail;
