import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { profileReducer, userReducer } from "./reducers/userReducer";
import { allOwnerRestaurentReducer, restaurentDetailsReducer, restaurentReducer } from "./reducers/restaurentReducer";
import { ownerDetailsReducer, usersReducer } from "./reducers/ownerReducers";


const reducer = combineReducers({
  user: userReducer,
  profile:profileReducer,
  restaurents:restaurentReducer,
  restaurentDetail:restaurentDetailsReducer,
  adminPanel:allOwnerRestaurentReducer,
  allUsers: usersReducer,
  ownerDetail:ownerDetailsReducer,
});
let initialState = {};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);



export default store;