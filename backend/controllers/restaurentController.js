const catchAsyncError = require("../middleware/catchAsyncError");
const Restaurent = require("../models/restaurentModel");
const User = require("../models/userModel");
const ErrorHandler = require("../utils/errorhandler");
const mongoose = require("mongoose");
const ApiFeature = require("../utils/ApiFeature");

exports.createRestaurents = catchAsyncError(async (req, res, next) => {
  const { restaurent } = req.body;
  console.log(restaurent);
  restaurent.city = restaurent.city.toLowerCase();

  const restaurentRow = await Restaurent.findOne({
    user: req.user.id,
    previousOwner: null,
  });

  if (restaurentRow) {
    restaurentRow.restaurent.push(restaurent);
    restaurentRow.numberOfRestaurent = restaurentRow.restaurent.length;
    restaurentRow.save();
  } else {
    await Restaurent.create({
      user: req.user.id,
      numberOfRestaurent: 1,
      restaurent,
    });
  }
  res.status(200).json({
    success: true,
    message: "restaurent Created",
    data: {
      restaurents: restaurentRow,
      restaurentsCount: restaurentRow.length,
    },
  });
});

//get all restaurents  --- Admin
exports.getAllRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find();
  res.status(200).json({
    success: true,
    restaurents,
  });
});

//get your own restaurent
exports.getOwnRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find({ user: req.user.id });
  if(!restaurents){
    return next(new ErrorHandler("No restaurent found"))
  }

  const restaurentArray=[];
  if(restaurents.length>1){
      restaurents.map(resta=>{
        resta.restaurent.map(item=>{
          restaurentArray.push(item);
        })
      })
  }else{
    restaurents[0].restaurent.map(resta=>{
      restaurentArray.push(resta)
    })
  }
  console.log(restaurentArray)
  res.status(200).json({
    success: true,
    restaurents: restaurentArray,
  });
});

//get all active restaurent - user admin  owner
exports.getAllActiveRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find();

  const allRestaturent = [];
  restaurents.map((res) => {
    res.restaurent.map((rest) => {
      allRestaturent.push({ owner: res.user, rest });
    });
  });
  
  const ActiveRestaurents = allRestaturent.filter(
    (res) => res.rest.status === "active"
  );
  console.log(ActiveRestaurents)

  const filterCityRestaurent = new ApiFeature(
    ActiveRestaurents,
    req.query
  ).search();
  const filterPriceRestaurent = new ApiFeature(
    filterCityRestaurent,
    req.query
  ).filterPrice();

  console.log(filterPriceRestaurent);

  res.status(200).json({
    success: true,
    restaurents: filterPriceRestaurent,
    restaurentsCount: filterPriceRestaurent.length,
  });
});
//get Restaurent Details
exports.getRestaurentDetails = catchAsyncError(async (req, res, next) => {
  const userId = req.params.uid;
  const restaurentId = req.params.rid;

  const ownerRestaurent = await Restaurent.find({ user: userId });
  if (!ownerRestaurent) {
    return next(new ErrorHandler("Owner with this id is not present"));
  }
  console.log(restaurentId);
  const rest = await ownerRestaurent[0].restaurent.find(
    (rest) => rest.id === restaurentId
  );
  console.log(rest);
  if (!rest) {
    return next(new ErrorHandler("Restaurent with this id is not present"));
  }
  res.status(200).json({
    success: true,
    restaurent: rest,
  });
});

//delete own Restaurent
exports.deleteRestaurent = catchAsyncError(async (req, res, next) => {
  const restaurentId = req.body.rid;

  const restaurents = await Restaurent.find({
    user: req.user.id,
  });

  if (restaurents[0].restaurent.length === 0) {
    return next(new ErrorHandler("you don not own any restaurent"));
  }

  let findResta;
  restaurents.map((resta) => {
    findResta = resta.restaurent.find((rest) => rest.id === restaurentId);
    if (findResta) {
      resta.restaurent.remove(findResta);
      resta.numberOfRestaurent = resta.restaurent.length;
      resta.save();
    }
  });

  if (!findResta) {
    return next(
      new ErrorHandler("you don not own any restaurent with this id", 404)
    );
  }

  res.status(200).json({
    success: true,
    data: {
      restaurents: restaurents[0].restaurent,
      restaurentsCount: restaurents[0].restaurent.length,
    },
  });
});

//delete restaurent -- Admin
exports.deleteOwnerRestaurent = catchAsyncError(async (req, res, next) => {
  const { userId, restId } = req.body;

  console.log(restId,userId)
  const ownerRestaurent = await Restaurent.find({ user: userId });
  if (!ownerRestaurent) {
    return next(new ErrorHandler("Owner with this id is not present"));
  }

  const rest = ownerRestaurent[0].restaurent.find((rest) => rest.id === restId);
  if (!rest) {
    return next(new ErrorHandler("Restaurent with this id is not present"));
  }

  ownerRestaurent[0].numberOfRestaurent -= 1;

  ownerRestaurent[0].restaurent.remove(rest);
  ownerRestaurent[0].save();

  res
    .status(200)
    .json({
      success: true,
      data: {
        restaurents: ownerRestaurent[0].restaurent,
        restaurentsCount: ownerRestaurent[0].restaurent.length,
      },
    });
});

//edit restaurent By Owner
exports.editRestaurent = catchAsyncError(async (req, res, next) => {
  const restaurentId = req.body.rid;
  const { restaurentName, AverageDiningPrice, city, status } = req.body;
  console.log(restaurentId, restaurentName, AverageDiningPrice, city, status);
  const ownerRestaurent = await Restaurent.find({ user: req.user.id });

  if (ownerRestaurent[0].restaurent.length === 0) {
    return next(new ErrorHandler("you don not own any restaurent"));
  }
  let findResta;
  ownerRestaurent.map((resta) => {
    findResta = resta.restaurent.find((rest) => rest.id === restaurentId);
    if (findResta) {
      findResta.restaurentName = restaurentName;
      findResta.AverageDiningPrice = AverageDiningPrice;
      findResta.city = city;
      findResta.status = status;
      resta.save();
    }
  });
  if (!findResta) {
    return next(
      new ErrorHandler("Restaurent with this id is not present", 404)
    );
  }

  res.status(200).json({
    success: true,
    message: "Restaurent Updated",
    restaurents: ownerRestaurent[0].restaurent,
    restaurentsCount: ownerRestaurent[0].restaurent.length,
  });
});

exports.editOwnerRestaurent = catchAsyncError(async (req, res, next) => {
  const {userId} = req.body;
  const {restId} = req.body;
  const { restaurentName, AverageDiningPrice, city, status } = req.body;
  console.log(userId,restId)

  const ownerRestaurent = await Restaurent.find({ user: userId });
  if (!ownerRestaurent) {
    return next(new ErrorHandler("Owner with this id is not present"));
  }

  const rest = ownerRestaurent[0].restaurent.find((rest) => rest.id === restId);
  if (!rest) {
    return next(new ErrorHandler("Restaurent with this id is not present"));
  }
  rest.restaurentName = restaurentName;
  rest.AverageDiningPrice = AverageDiningPrice;
  rest.city = city;
  rest.status = status;
  ownerRestaurent[0].save();

  res
    .status(200)
    .json({ success: true, message: "Restaurent Updated By Admin" });
});
