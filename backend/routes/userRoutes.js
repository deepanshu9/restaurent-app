const express=require("express");
const { registerUser, getUserDetails, loginUser, logout, updatePassword, updateProfile, getAllUser, getSingleUser, updateUserRole, deleteUser } = require("../controllers/userController");
const { isAuthenticatedUser, authorizeRoles } = require("../middleware/auth");

const router=express.Router();

//register User
router.route("/register").post(registerUser)

//Login user
router.route("/login").post(loginUser);

//Logout User
router.route("/logout").get(logout);

//get Profile Details
router.route("/me").get(isAuthenticatedUser,getUserDetails);

//update Password
router.route("/password/update").put(isAuthenticatedUser,updatePassword);

//update Profile
router.route("/me/update").put(isAuthenticatedUser,updateProfile);

//get All user -- ADMIN
router
  .route("/admin/users")
  .get(isAuthenticatedUser, authorizeRoles("admin"), getAllUser);

//get single user Details ,  update user Role , Delete USer ---- Admin
router
  .route("/admin/user/:id")
  .get(isAuthenticatedUser, authorizeRoles("admin"), getSingleUser)
  .put(isAuthenticatedUser, authorizeRoles("admin"), updateUserRole)
  .delete(isAuthenticatedUser, authorizeRoles("admin"), deleteUser);

module.exports = router;